# credit to devdungeon whom I shamelessly stole most of this code from, super simple shit honestly
import random
import asyncio
import aiohttp
import json
from discord import Game
from discord.ext.commands import Bot
from math import log

def tismcalc(vo2,bp,ohp,sqt,dl,bpm,hght,age):
    wingspan = hght #surprisingly average wingspan:height ratio is 1:1
    avghght = 72 #arbitrary
    avgage = 25 #arbitrary
    maxvol = 6000 #average male has a lung capacity of 6liters, dyel
    result1 = 3*log(vo2/(bpm*maxvol))*log(hght-avghght)
    result2 = (+2*dl/(hght-wingspan/2)+2*(sqt+2*bp+ohp)/wingspan)*10^(age-avgage)
    return(result1*result2)




BOT_PREFIX = ("?", "!")
TOKEN = "NTI5MDY3NTY5OTgyOTk2NTIx.Dwrgtg.pRJmj6UoODyseM-k8SNlBXQan3w"  # Get at discordapp.com/developers/applications/me

client = Bot(command_prefix=BOT_PREFIX)

@client.command
async def help():
    await client.say("""As of right now I can only do the following:
!help: you're using it right now!
!hello: say hello!
!eightball: tells you your fortune
!wilks: given your weight and lifting total (bench,squat,deads summed up) in that order will serve you your wilks
!tism: given your vo2, bench, ohp, squat, deadlift, resting heartrate, height, and age will calculate your tism
!play; given a youtube url, will enter voicechat and play a video!
""")


@client.command()
async def hello():
    await client.say("Hello!")
@client.command(name='8ball',
                description="Answers a yes/no question.",
                brief="Answers from the beyond.",
                aliases=['eight_ball', 'eightball', '8-ball'],
                pass_context=True)
async def eight_ball(context):
    possibleresponses = [
        'That is a resounding no',
        'It is not looking likely',
        'Too hard to tell',
        'It is quite possible',
        'Definitely',
    ]
    await client.say(random.choice(possible_responses) + ", " + context.message.author.mention)


@client.command()
async def wilks(weight,total):
    a = -216.0475144
    b = 16.2606339 
    c = -0.002388645
    d = -0.00113732
    e = 0.00000701863
    f = -1.291*10^(-8)
    total *= 1/2.2
    weight *= 1/2.2
    wilk = total*500/(a+b*weight^2+c*weight^3+d*weight^4+e*weight^5+f*weight^6)
    await client.say("your wilks is: "+str(wilk))


@client.event
async def on_ready():
    await client.change_presence(game=Game(name="with humans"))
    print("Logged in as " + client.user.name)

@client.command()
async def tism(vo2,bp,ohp,sqt,dl,bpm,hght,age):
    tisms = tismcalc(vo2,bp,ohp,sqt,dl,bpm,hght,age)
    await client.say("Your tism is: "+str(tisms))

@client.command(pass_context=True)
async def play(ctx, url):
    url = ctx.message.content
    url = url.strip('!play ')

    author = ctx.message.author
    voice_channel = author.voice_channel
    vc = await client.join_voice_channel(voice_channel)

    player = await vc.create_ytdl_player(url)
    player.start()

async def list_servers():
    await client.wait_until_ready()
    while not client.is_closed:
        print("Current servers:")
        for server in client.servers:
            print(server.name)
        await asyncio.sleep(600)


client.loop.create_task(list_servers())
client.run(TOKEN)